import React from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Route, Switch} from 'react-router-dom';

// Styles
import 'simple-line-icons/css/simple-line-icons.css';
import '../scss/style.scss'

// Containers
import Main from './views/pages/Main/'

// Views
import Page404 from './views/pages/Page404/'
import Page500 from './views/pages/Page500/'

ReactDOM.render((
  <HashRouter>
    <Switch>
      <Route exact path="/404" name="Page 404" component={Page404}/>
      <Route exact path="/500" name="Page 500" component={Page500}/>
      <Route path="/" name="Home" component={Main}/>
    </Switch>
  </HashRouter>
), document.getElementById('root'));
