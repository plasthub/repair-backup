import React, {Component} from 'react';

class Download extends Component {
  render() {
    return (
        <section id="breakout" className="breaking centered" className="breaking" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="40">

            <div className="color-overlay">

                <div className="breaking-content">

                    <div className="container">
                        <div className="row">

                            <div className="col-lg-12 centered">

                                <h4>Download Repair365 App from Apple App Store and Google Play Store</h4>

                                <p className="sub-lead">Go capture your moment of life and share it with friends</p>

                                <div className="clearfix"></div>

                                <ul className="inline-cta">

                                    <li>
                                        <a href="#0" className="store-btn"><img src="img/theme/app/appstore-btn.png" alt="Appstore"/></a>
                                    </li>

                                    <li>
                                        <a href="#0" className="store-btn"><img src="img/theme/app/playstore-btn.png" alt="Playstore"/></a>
                                    </li>

                                </ul> 

                                <small><em>* Repair365 App is 100% private. Only you and person you
                                    approved can see your moments</em></small>

                            </div> 

                        </div> 
                    </div> 

                </div>

            </div> 

        </section>
    )
  }
}

export default Download;
