import React, {Component} from 'react';

class Subscribe extends Component {
  render() {
    return (
        <section id="subscribe-section" className="breaking" className="breaking" data-stellar-background-ratio="0.2" data-stellar-vertical-offset="0">

            <div className="color-overlay">

                <div className="breaking-content">

                    <div className="container">
                        <div className="row">

                            <div className="col-lg-3 col-lg-offset-2 col-md-3 col-md-offset-2 col-sm-4 centered">

                                <img className="subscribe-icon" src="img/theme/placeholder/132x149.png" alt="Open Envelope Icon"/>

                            </div> 

                            <div className="subscribe-section-content col-lg-5 col-md-5 col-sm-7">

                                <h4>Subscribe For Updates</h4>

                                <p>Join our 622,314 subscribers and get access to the latest tools, freebies, product announcements and much more!</p>

                                <form id="footer-subscribe" className="the-subscribe-form">

                                    <div className="input-group">
                                        <input type="email" className="form-control" placeholder="Enter Email Address"/>
                                        <span className="input-group-btn">
                                            <button className="btn btn-subscribe all-caps" type="submit">Subscribe</button>
                                        </span>
                                    </div>

                                </form> 

                            </div> 

                        </div> 
                    </div>

                </div> 

            </div>
        </section>
    )
  }
}

export default Subscribe;
