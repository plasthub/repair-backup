import React, {Component} from 'react';

class Counters extends Component {
  render() {
    return (
        <section id="counter" className="breaking centered" className="breaking" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="40">

            <div className="color-overlay">

                <div className="breaking-content">

                    <div className="container">
                        <div className="row">

                            <div className="col-lg-3 col-md-3 col-sm-3">

                                <span className="counter-icon icon-emotsmile repair365-orange-color"></span>
                                <span className="counter">12345</span>
                                <span className="counter-title">Happy Customers</span> 

                            </div>

                            <div className="col-lg-3 col-md-3 col-sm-3">

                                <span className="counter-icon icon-lock repair365-orange-color"></span>
                                <span className="counter">12345</span> 
                                <span className="counter-title">Data Savings</span> 

                            </div>

                            <div className="col-lg-3 col-md-3 col-sm-3">

                                <span className="counter-icon icon-envelope repair365-orange-color"></span>
                                <span className="counter">12345</span> 
                                <span className="counter-title">Subscribers</span> 

                            </div> 

                            <div className="col-lg-3 col-md-3 col-sm-3">

                                <span className="counter-icon icon-globe repair365-orange-color"></span>
                                <span className="counter">12345</span> 
                                <span className="counter-title">Global Supports</span> 

                            </div> 

                        </div> 
                    </div> 

                </div> 

            </div> 

        </section>
    )
  }
}

export default Counters;
