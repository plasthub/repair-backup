import React, {Component} from 'react';

class Plan extends Component {
  render() {
    return (
        <section id="pricing" className="centered">

            <div className="container">
                <div className="row">

                    <div className="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 centered">

                        <p className="section-title">Pricing Tables</p>

                        <h2 className="section-heading">Select the plan that suits you.<br/>Upgrade, downgrade, or cancel anytime.</h2>

                    </div> 

                    <div className="clearfix"></div>

                    <ul className="pricing">

                        <div className="col-lg-3 col-md-3 col-sm-3 col-sm-offset-0 col-xs-8 col-xs-offset-2">
                            <li className="price">

                                <h5 className="price-title">Bronze</h5>
                                <div className="price-amount">$25</div>
                                <ul className="price-feature">
                                    <li>1 Maecenas sed diam eget risus varius blandit</li>
                                    <li>200 Cras justo odio dapibus quam</li>
                                    <li>UNLIMITED facilisis faucibus dolor auctor</li>
                                    <li>Vivamus sagittis lacus vel augue laoreet rutrum</li>
                                </ul> 
                                <a href="#" className="price-button all-caps">Buy Now</a>

                            </li> 
                        </div> 

                        <div className="col-lg-3 col-md-3 col-sm-3 col-sm-offset-0 col-xs-8 col-xs-offset-2">
                            <li className="price">

                                <h5 className="price-title">Silver</h5>
                                <div className="price-amount">$35</div>
                                <ul className="price-feature">
                                    <li>2 Maecenas sed diam eget risus varius blandit</li>
                                    <li>250 Cras justo odio dapibus quam</li>
                                    <li>UNLIMITED facilisis faucibus dolor auctor</li>
                                    <li>Vivamus sagittis lacus vel augue laoreet rutrum</li>
                                </ul> 
                                <a href="#" className="price-button all-caps">Buy Now</a>

                            </li> 
                        </div> 

                        <div className="col-lg-3 col-md-3 col-sm-3 col-sm-offset-0 col-xs-8 col-xs-offset-2">
                            <li className="price best-value">

                                <h5 className="price-title">Gold</h5>
                                <span className="best-value-label">best value</span>
                                <div className="price-amount">$50</div>
                                <ul className="price-feature">
                                    <li>3 Maecenas sed diam eget risus varius blandit</li>
                                    <li>400 Cras justo odio dapibus quam</li>
                                    <li>UNLIMITED facilisis faucibus dolor auctor</li>
                                    <li>Vivamus sagittis lacus vel augue laoreet rutrum</li>
                                    <li>Sed blandit non mi <span className="price-label">Free</span></li>
                                </ul> 
                                <a href="#" className="price-button all-caps">Buy Now</a>

                            </li> 
                        </div> 

                        <div className="col-lg-3 col-md-3 col-sm-3 col-sm-offset-0 col-xs-8 col-xs-offset-2">
                            <li className="price">

                                <h5 className="price-title">Platinum</h5>
                                <div className="price-amount">$75</div>
                                <ul className="price-feature">
                                    <li>2 Maecenas sed diam eget risus varius blandit</li>
                                    <li>450 Cras justo odio dapibus quam</li>
                                    <li>UNLIMITED facilisis faucibus dolor auctor</li>
                                    <li>Vivamus sagittis lacus vel augue laoreet rutrum <span className="price-label">Free</span></li>
                                </ul> 
                                <a href="#" className="price-button all-caps">Buy Now</a>

                            </li> 
                        </div> 

                    </ul> 

                </div> 
            </div> 

        </section>
    )
  }
}

export default Plan;
