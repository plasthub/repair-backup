import React, {Component} from 'react';

class Breakout extends Component {
    render() {
        return (
            <section id="breakout" className="breaking centered" className="breaking" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="40">

                <div className="color-overlay">

                    <div className="breaking-content">

                        <div className="container">
                            <div className="row">

                                <div className="col-lg-12 centered">

                                    <h4>Still confused choosing the right plan?</h4>
                                    <p className="sub-lead">Register now and get free access to all of our premium services for 7 days.</p>

                                    <a href="#0" className="cta cta-stroke all-caps contact-trigger">Register Now</a>
                                    <div className="clearfix"></div>
                                    <small><em>*no credit card required</em></small>

                                </div>

                            </div>
                        </div>

                    </div> 

                </div>

            </section>
        )
    }
}

export default Breakout;
