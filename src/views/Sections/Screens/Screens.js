import React, {Component} from 'react';

class Screens extends Component {
  render() {
    return (
        <section id="screenshots">

            <div className="container">
                <div className="row">

                    <div className="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 centered">

                        <p className="section-title">App Screenshots</p>

                        <h2 className="section-heading">See how our app is designed in beauty and simplicity.</h2>

                    </div> 

                    <div className="clearfix"></div>


                </div> 
            </div>

            <div className="container-full">

                <div className="row">

                    <div className="col-lg-12 no-padding">

                        <div className="app-carousel">

                            <div className="carousel-item">
                                <img src="img/theme/placeholder/295x524.png" alt="Screenshot" />
                            </div> 

                            <div className="carousel-item">
                                <img src="img/theme/placeholder/295x524.png" alt="Screenshot" />
                            </div> 

                            <div className="carousel-item">
                                <img src="img/theme/placeholder/295x524.png" alt="Screenshot" />
                            </div> 

                            <div className="carousel-item">
                                <img src="img/theme/placeholder/295x524.png" alt="Screenshot" />
                            </div> 

                            <div className="carousel-item">
                                <img src="img/theme/placeholder/295x524.png" alt="Screenshot" />
                            </div> 

                            <div className="carousel-item">
                                <img src="img/theme/placeholder/295x524.png" alt="Screenshot" />
                            </div> 

                            <div className="carousel-item">
                                <img src="img/theme/placeholder/295x524.png" alt="Screenshot" />
                            </div> 

                            <div className="carousel-item">
                                <img src="img/theme/placeholder/295x524.png" alt="Screenshot" />
                            </div> 

                            <div className="carousel-item">
                                <img src="img/theme/placeholder/295x524.png" alt="Screenshot" />
                            </div> 

                            <div className="carousel-item">
                                <img src="img/theme/placeholder/295x524.png" alt="Screenshot" />
                            </div> 

                            <div className="carousel-item">
                                <img src="img/theme/placeholder/295x524.png" alt="Screenshot" />
                            </div> 

                        </div> 

                        <div className="phone-frame">
                            <img src="img/theme/mockup/iphone-app-frame.png" alt="iPhone Frame"/>
                        </div> 

                    </div> 

                </div> 
            </div> 

        </section>
    )
  }
}

export default Screens;
