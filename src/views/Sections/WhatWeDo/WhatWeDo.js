import React, {Component} from 'react';

class WhatWeDo extends Component {
  render() {
    return (
        <section id="what-we-do" className="centered">

            <div className="container">
                <div className="row">

                    {/*<div className="col-lg-12">*/}

                        {/*<div className="attention-box">*/}
                            {/*<h5 className="all-caps">Hello!!</h5>*/}
                            {/*<p><strong>Attention Grabber -</strong> this notification would be clearly seen by your visitor and drive convertions.</p>*/}
                            {/*<a href="#" className="more">Learn More</a>*/}
                        {/*</div> */}

                    {/*</div>*/}

                    <div className="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 centered">

                        <p className="section-title">What We Do</p>

                        <h2 className="section-heading">We make your business gain more revenue at a glance.</h2>

                    </div>

                    <div className="clearfix"></div>

                    <div className="col-lg-10 col-lg-offset-1">

                        <p><strong>Maecenas sed diam eget</strong> risus varius blandit sit amet non magna. Morbi leo risus, porta ac consectetur ac, 
                            vestibulum at eros. Vestibulum id ligula porta felis euismod semper. <strong>Nulla vitae elit libero</strong>, 
                            a pharetra augue. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis <a href="#">vestibulum</a>. 
                            Maecenas mollis interdum!</p>

                    </div>

                    <div className="clearfix"></div>

                    <a href="#" className="cta cta-default all-caps contact-trigger"> Get in Touch</a>

                </div>
            </div>

        </section>
    )
  }
}

export default WhatWeDo;
