import React, {Component} from 'react';

class Highlights extends Component {
  render() {
    return (
        <section id="featurettes" className="expandable-gallery gray-bg no-padding">

            <div className="container-full">
                <div className="row">

                    
                    <div className="expandable-gallery-wrapper col-lg-6 col-md-6 no-padding">

                        <ul className="expandable-gallery-item">
                            <li className="selected"><img src="img/theme/stocks/stock1.jpg" alt="Photo Stocks"/></li>
                            {/*<li><img src="img/theme/stocks/tool-hammer-repair-master-162644.jpeg" alt="Photo Stocks"/></li>*/}
                            {/*<li><img src="img/theme/stocks/tool-hammer-repair-master-162644.jpeg" alt="Photo Stocks"/></li>*/}
                        </ul> 

                        {/*<ul className="expandable-gallery-nav">*/}
                            {/*<li><a href="#" className="expand-prev inactive">Next</a></li>*/}
                            {/*<li><a href="#" className="expand-next">Prev</a></li>*/}
                        {/*</ul> */}

                        {/*<a href="#" className="expandable-close">Close</a>*/}

                    </div> 

                    <div className="expandable-gallery-info two-blocks-col col-lg-6 col-md-6">

                        <h3>Other things you can do</h3>

                        <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. </p>

                        <ul className="checklist">
                            <li>Nullam id dolor id nibh ultricies vehicula ut id elit.</li>
                            <li>Aenean lacinia bibendum nulla sed consectetur.</li>
                            <li>Nulla vitae elit libero, a pharetra augue.</li>
                            <li>Etiam porta sem malesuada magna mollis euismod a pharetra augue.</li>
                            <li className="visible-lg">Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</li>
                        </ul> 

                        <a href="#" className="more">Read More</a>

                    </div> 

                    <div className="clearfix"></div>

                    
                    <div className="two-blocks-col col-lg-6 col-md-6">

                        <h3>Content tab example</h3>

                        
                        <div className="content-tab-wrapper" role="tabpanel">

                            
                            <ul className="nav nav-tabs" role="tablist">
                                <li role="presentation" className="active"><a href="#first" aria-controls="first" role="tab" data-toggle="tab"><span className="icon-settings"></span>Settings</a></li>
                                <li role="presentation"><a href="#second" aria-controls="second" role="tab" data-toggle="tab"><span className="icon-cloud-upload"></span>Upload</a></li>
                                <li role="presentation"><a href="#third" aria-controls="second" role="tab" data-toggle="tab"><span className="icon-chart"></span>Analytics</a></li>
                                <li role="presentation"><a href="#fourth" aria-controls="third" role="tab" data-toggle="tab"><span className="icon-drawer"></span>Archive</a></li>
                                <li role="presentation"><a href="#fifth" aria-controls="fourth" role="tab" data-toggle="tab"><span className="icon-share"></span>Sharing</a></li>
                            </ul>
                           


                           
                            <div className="tab-content">

                                <div role="tabpanel" className="tab-pane fade in active" id="first">
                                    <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

                                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>

                                    <a href="#" className="more">Read More</a>
                                </div> 

                                <div role="tabpanel" className="tab-pane fade" id="second">
                                    <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

                                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>

                                    <a href="#" className="more">Read More</a>
                                </div> 

                                <div role="tabpanel" className="tab-pane fade" id="third">
                                    <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

                                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>

                                    <a href="#" className="more">Read More</a>
                                </div> 

                                <div role="tabpanel" className="tab-pane fade" id="fourth">
                                    <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

                                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>

                                    <a href="#" className="more">Read More</a>
                                </div> 

                                <div role="tabpanel" className="tab-pane fade" id="fifth">
                                    <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>

                                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>

                                    <a href="#" className="more">Read More</a>
                                </div> 

                            </div>
                            

                        </div> 

                    </div> 

                    <div className="featurettes-quote-wrapper col-lg-6 col-md-6 no-padding">

                        <div className="vertical-center-wrapper">

                            <div className="vertical-center-table">

                                <div className="vertical-center-content">

                                    <div className="featurettes-quote centered">

                                        Donec id elit non mi porta gravida at eget metus. Donec ullamcorper nulla non metus auctor fringilla. 
                                        Integer posuere erat a ante venenatis dapibus posuere velit aliquet.

                                        <h4 className="featurettes-quote-author">- John Doe</h4>

                                    </div>

                                </div> 

                            </div> 

                        </div> 

                    </div> 


                </div> 
            </div> 

        </section>
    )
  }
}

export default Highlights;
