import React, {Component} from 'react';
import withScrollReveal from 'react-scrollreveal'

class Clients extends Component {
  render() {
      const { animationContainerReference } = this.props;

    return (
        <section id="top-client" className="centered">

            <div className="container">
                <div className="row">

                    <div className="client-logo">
                        <div className="the-logo col-lg-2 col-md-2">

                            <h5 className="top-logo-text all-caps">Trusted By </h5>

                        </div>

                        <div className="the-logo col-lg-2 col-md-2 col-sm-3 col-xs-3">

                            <a href="#"><img src="img/theme/app/client.png" alt="Client Logo" className="sr-item" ref={animationContainerReference}/></a>

                        </div>

                        <div className="the-logo col-lg-2 col-md-2 col-sm-3 col-xs-3">

                            <a href="#"><img src="img/theme/app/client.png" alt="Client Logo" ref={animationContainerReference}/></a>

                        </div>

                        <div className="the-logo col-lg-2 col-md-2 col-sm-3 col-xs-3">

                            <a href="#"><img src="img/theme/app/client.png" alt="Client Logo" ref={animationContainerReference}/></a>

                        </div>

                        <div className="the-logo col-lg-2 col-md-2 col-sm-3 col-xs-3">

                            <a href="#"><img src="img/theme/app/client.png" alt="Client Logo" ref={animationContainerReference}/></a>

                        </div>

                        <div className="the-logo col-lg-2 col-md-2 hidden-sm hidden-xs">

                            <a href="#"><img src="img/theme/app/client.png" alt="Client Logo" ref={animationContainerReference}/></a>

                        </div>

                    </div>


                </div>
            </div>

        </section>
    )
  }
}

// export default Clients;



export default withScrollReveal([
    {
        selector: '.sr-item',
        options: {
            reset: true,
        },
    },
    {
        selector: '.sr-item--sequence',
        options: {
            reset: true,
            delay: 2000,
        },
        interval: 100
    }
])(Clients)


