import React, {Component} from 'react';

class Features extends Component {
  render() {
    return (
        <section id="our-features">

            <div className="container">
                <div className="row">

                    <div className="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 centered margin-bot-5">

                        <p className="section-title">Our Features</p>

                        <h2 className="section-heading">Enjoy the next level experiences you can get from our app.</h2>

                    </div>

                    <div className="clearfix"></div>


                    <div className="col-lg-4 col-md-4 col-sm-12 left-features">

                        <div className="row">

                            <div className="the-feature col-lg-12 col-md-12 col-sm-4 col-xs-12">

                                <a href="#">
                                    <span className="feature-icon icon-layers"></span>

                                    <h4 className="feature-title">Bootstrap 3.3.2</h4>

                                    <p>Etiam porta sem malesuada magna mollis euismod eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                                </a>

                            </div>

                            <div className="the-feature col-lg-12 col-md-12 col-sm-4">

                                <a href="#">
                                    <span className="feature-icon icon-trophy"></span>

                                    <h4 className="feature-title">Professional Design</h4>

                                    <p>Etiam porta sem malesuada magna mollis euismod eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                                </a>

                            </div>

                            <div className="the-feature col-lg-12 col-md-12 col-sm-4">

                                <a href="#">
                                    <span className="feature-icon icon-bulb"></span>

                                    <h4 className="feature-title">High Conversion</h4>

                                    <p>Etiam porta sem malesuada magna mollis euismod eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                                </a>

                            </div>

                        </div>

                    </div>


                    <div className="col-lg-4 col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3 col-xs-6 col-xs-offset-3">

                        <div className="middle-phone-mockup">

                            <img src="img/theme/mockup/iphone-front-features.png" alt="iPhone Mockup"/>

                        </div>

                    </div>



                    <div className="col-lg-4 col-md-4 col-sm-12 col-xs-12 right-features">

                        <div className="row">

                            <div className="the-feature col-lg-12 col-md-12 col-sm-4">

                                <a href="#">
                                    <span className="feature-icon icon-screen-smartphone"></span>

                                    <h4 className="feature-title">Fully Responsive</h4>

                                    <p>Etiam porta sem malesuada magna mollis euismod eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                                </a>

                            </div>

                            <div className="the-feature col-lg-12 col-md-12 col-sm-4">

                                <a href="#">
                                    <span className="feature-icon icon-equalizer"></span>

                                    <h4 className="feature-title">Customization</h4>

                                    <p>Etiam porta sem malesuada magna mollis euismod eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                                </a>

                            </div>

                            <div className="the-feature col-lg-12 col-md-12 col-sm-4">

                                <a href="#">
                                    <span className="feature-icon icon-globe-alt"></span>

                                    <h4 className="feature-title">Cross Browser Compatible</h4>

                                    <p>Etiam porta sem malesuada magna mollis euismod eget metus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
                                </a>

                            </div>

                        </div>

                    </div>

                </div>
            </div>

        </section>
    )
  }
}

export default Features;
