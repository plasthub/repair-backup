import React, {Component} from 'react';

class WhyUs extends Component {
  render() {
    return (
        <div>
            <section id="why-us">

                <div className="container">
                    <div className="row">

                        <div className="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 centered">

                            <p className="section-title">Why Choose Us</p>

                            <h2 className="section-heading">Because other apps are either money waste or way to complex.</h2>

                        </div>

                        <div className="clearfix"></div>

                        <div className="col-lg-6 col-md-6 col-sm-12 col-sm-offset-0 col-xs-8 col-xs-offset-2">

                            <div className="half-phone-mockup">
                                <img className="opacity-one" src="img/theme/mockup/half-iphone-side-right.png" alt="iPhone Side 1" />
                            </div>

                        </div>

                        <div className="col-lg-6 col-md-6">

                            <div className="why-us-content">

                                <span className="why-us-icon icon-anchor"></span>

                                <h3>Stay organized. Access and edit your account from all over the world.</h3>

                                <p>Sed posuere consectetur est at lobortis. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Cras justo odio,
                                    dapibus ac facilisis in, egestas eget quam. Nullam quis risus eget urna mollis ornare vel eu leo. Integer posuere
                                    erat a ante venenatis dapibus posuere velit aliquet.</p>

                                <p>Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper. Nullam quis risus
                                    eget urna mollis ornare vel eu leo.</p>

                            </div>

                        </div>

                    </div>
                </div>

            </section>


            <section id="why-us-2" className="gray-bg">

                <div className="container">
                    <div className="row">

                        <div className="col-lg-6 col-md-6">

                            <div className="why-us-content">

                                <span className="why-us-icon icon-microphone"></span>

                                <h3>Work on any platforms. The one that can make sharing with others more enjoyable.</h3>

                                <p>Sed posuere consectetur est at lobortis. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Cras
                                    justo odio, dapibus ac facilisis in, egestas eget quam. Nullam quis risus eget urna mollis ornare vel eu leo.
                                    Integer posuere erat a ante venenatis dapibus posuere velit aliquet.</p>

                                <p>Donec id elit non mi porta gravida at eget metus. Vestibulum id ligula porta felis euismod semper. Nullam quis
                                    risus eget urna mollis ornare vel eu leo.</p>

                            </div>

                        </div>

                        <div className="col-lg-6 col-md-6 col-sm-12 col-sm-offset-0 col-xs-8 col-xs-offset-2">

                            <div className="half-phone-mockup zero-bottom">
                                <img className="opacity-one" src="img/theme/mockup/half-iphone-side-left.png" alt="iPhone Side 2"/>
                            </div>

                        </div>

                    </div>
                </div>

            </section>
        </div>
    )
  }
}

export default WhyUs;
