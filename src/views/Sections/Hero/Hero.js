import React, {Component} from 'react';

class Hero extends Component {
  render() {
    return (
        <section id="hero" className="breaking" data-stellar-background-ratio="0.5" data-stellar-vertical-offset="30">

            <div className="container">

                <div className="vertical-center-wrapper">
                    <div className="vertical-center-table">
                        <div className="vertical-center-content">

                            <div className="hero-content row centered">
                                <div className="col-lg-12">

                                    <p className="lead all-caps text-shadow-small margin-bot-30">
                                        You Break We Fix
                                    </p>
                                    <h1 className="all-caps text-shadow-medium">A Perfect Landing Page Template For</h1>
                                    <h1 className="all-caps animated-headline slide">
											<span className="animated-words-wrapper">
												<b className="is-visible">repair service center</b>
												<b>Creative Agency</b>
												<b>All Business Purpose</b>
											</span>
                                    </h1>

                                    <ul className="inline-cta">

                                        <li>
                                            <a href="#" className="cta cta-default all-caps">Learn More</a>
                                        </li>

                                        <li>
                                            <a href="#" className="cta cta-default cta-orange all-caps contact-trigger">Get in Touch</a>
                                        </li>

                                    </ul>

                                    <a href="#what-we-do" className="scroll-to scroll-link">
                                        <span className="icon-arrow-down"></span>
                                    </a>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>

        </section>
    )
  }
}

export default Hero;
