import React, {Component} from 'react';

// Components
import Header from "../../../components/Header/";
import NavMenu from "../../../components/NavMenu/NavMenu";
import Footer from "../../../components/Footer/";

// Sections
import Hero from "../../Sections/Hero/Hero";
import Clients from "../../Sections/Clients/Clients";
import WhatWeDo from "../../Sections/WhatWeDo/WhatWeDo";
import Features from "../../Sections/Features/Features";
import Highlights from "../../Sections/Highlights/Highlights";
import Counters from "../../Sections/Counters/Counters";
import WhyUs from "../../Sections/WhyUs/WhyUs";
import Download from "../../Sections/Download/Download";
import Screens from "../../Sections/Screens/Screens";
import Subscribe from "../../Sections/Subscribe/Subscribe";
import Breakout from "../../Sections/Breakout/Breakout";
import Plan from "../../Sections/Plan/Plan";

class Main extends Component {
  render() {
    return (
      <div>
          <Header/>
          <NavMenu/>
          <main id="main-content" className="creative-layout">
              <Hero/>
              <Clients/>
              <WhatWeDo/>
              <Breakout/>
              <Features/>
              <Highlights/>
              <Counters/>
              <WhyUs/>
              <Download/>
              <Plan/>
              {/*<Screens/>*/}
              <Subscribe/>
          </main>
          <Footer/>
      </div>
    );
  }
}

export default Main;
