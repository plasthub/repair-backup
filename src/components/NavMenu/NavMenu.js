import React, {Component} from 'react';

class NavMenu extends Component {
    render() {
        return (
            <nav id="nav-wrapper">

                <a className="nav-close" href="#"><span className="icon-close"></span></a>

                <ul id="main-nav" className="main-nav all-caps">
                    <li className="current"><a href="#hero">Home</a></li>
                    <li><a href="#what-we-do">What We Do</a></li>
                    <li><a href="#our-features">Our Features</a></li>
                    <li><a href="#portfolio">Portfolio</a></li>
                    <li><a href="#our-team">The Team</a></li>
                    <li><a href="#customer-story">Customers</a></li>
                    <li className="dropdown">
                        <a className="external" href="#">
                            Dropdown
                        </a>
                        <ul className="dropdown-menu">
                            <li><a href="#">Some Submenu</a></li>
                            <li><a href="#">And More</a></li>
                        </ul>
                    </li>
                </ul>

                <ul className="secondary-nav">
                    <li><a href="#">About This Template</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Terms of Service</a></li>
                    <li><a href="#">Legal</a></li>
                    <li><a href="#">Careers</a></li>
                    <li><a className="contact-trigger">Contact Us</a></li>
                </ul>

            </nav>
        )
    }
}

export default NavMenu;
