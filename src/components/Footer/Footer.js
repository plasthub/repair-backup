import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
        <footer id="main-footer" className="app-layout centered">
            <div className="container">
                <div className="row">
                    <div className="footer-content col-lg-12 centered">
                        <img className="margin-bot-40" src="img/theme/placeholder/128x128.png" alt="Footer App Icon" />
                        <p className="lead margin-top-8 margin-bot-25">Probably <strong>the best</strong> landing page for your apps</p>
                        <p>Available for iOS 7+ and Android Jelly Bean+</p>
                        <ul className="inline-cta">
                            <li>
                                <a href="#" className="store-btn"><img src="img/theme/app/appstore-btn.png" alt="Appstore"/></a>
                            </li>
                            <li>
                                <a href="#" className="store-btn"><img src="img/theme/app/playstore-btn.png" alt="Playstore"/></a>
                            </li>
                        </ul>
                    </div>

                    <div className="copyright">
                        <p><a href="https://modularview.com">Modular View</a> &copy; 2015 Repair365.</p>
                        <div className="language">
                            <span className="icon-globe"></span>
                            <a tabIndex="0"
                               role="button"
                               data-toggle="popover"
                               data-trigger="focus"
                               data-html="true"
                               data-placement="top"
                               data-content="
									<ul className='language-selection'>
										<li><a href='#'>Dansk</a></li>
										<li><a href='#'>Deutsch</a></li>
										<li><a href='#'>Espanol</a></li>
										<li><a href='#'>Francais</a></li>
									</ul>"
                            >English (US) <span className="icon-arrow-up"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
  }
}

export default Footer;
