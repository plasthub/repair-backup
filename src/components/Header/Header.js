import React, {Component} from 'react';

class Header extends Component {

  constructor(props) {
    super(props);
  }
  render() {
    return (
        <header id="main-header" className="the-header the-origin-header">

            <div className="container">
                <div className="row">

                    <div className="col-lg-12">

                        <a href="/" className="logo"><img src={'img/theme/logo.png'} alt="Repair365"/></a>

                        <a href="#" id="nav-menu-trigger" className="menu-toggle flip pull-right all-caps">Menu<span className="icon-menu"></span></a>

                    </div>

                </div>
            </div>

        </header>
    );
  }
}

export default Header;
